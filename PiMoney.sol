pragma solidity 0.5.0;

import "./safeMath.sol"; //https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/math/safeMath.sol
import "./IRC223.sol"; //https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/token/ERC20/IERC20.sol
import "./ERC223_receiving_contract.sol";
import "./Owned.sol";

contract PiMoney is Owned, ERC223ReceivingContract {
    using SafeMath for uint;
    
    mapping(address => mapping(address => uint)) public balances;
    mapping(address => uint) public nonceByUser;
    mapping(address => uint) public cummulatedCommissions;
    
    uint public commissionSend;
    uint public commissionPay;
    
    event Send(address indexed tokenAddress, address indexed sender, uint nonce, address indexed receiver, uint toSend, uint commission, uint time);
    event Pay(address indexed tokenAddress, address indexed sender, uint nonce, address indexed receiver, uint toSend, uint commission, uint time);
    
    constructor(uint _commissionSend, uint _commissionPay) public {
        commissionSend = _commissionSend;
        commissionPay = _commissionPay;
    }
    
    function send(address payable to, uint value) external payable {
        uint _commission = value.mul(commissionSend).div(100 ether);
        _transfer(msg.sender, to, value, _commission);
        cummulatedCommissions[address(0)] = cummulatedCommissions[address(0)].add(_commission);
        
        emit Send(address(0), msg.sender, nonceByUser[msg.sender], to, value, _commission, now);
    }
    
    function pay(address payable to, uint value) external payable {
        uint _commission = value.mul(commissionPay).div(100 ether);
        _transfer(msg.sender, to, value, _commission);
        cummulatedCommissions[address(0)] = cummulatedCommissions[address(0)].add(_commission);
        
        emit Pay(address(0), msg.sender, nonceByUser[msg.sender], to, value, _commission, now);
    }
    
    /// @dev Standard ERC223 function that will handle incoming token transfers
    /// @param _from Token sender address.
    /// @param _value Amount of tokens.
    function tokenFallback(address payable _from, uint _value) public {
        balances[_from][msg.sender] = balances[_from][msg.sender].add(_value);
    }
    
    function changeCommission(uint newCommissionSend, uint newCommissionPay) public onlyOwner {
        commissionSend = newCommissionSend;
        commissionPay = newCommissionPay;
    }
    
    function sendToken(address tokenAddress, address to, uint value) public {
        uint _commission = value.mul(commissionSend).div(100 ether);
        _transferToken(msg.sender, tokenAddress, to, value, _commission);
        cummulatedCommissions[tokenAddress] = cummulatedCommissions[tokenAddress].add(_commission);
        
        emit Send(tokenAddress, msg.sender, nonceByUser[msg.sender], to, value, _commission, now);
    }
    
    function payToken(address tokenAddress, address to, uint value) public {
        uint _commission = value.mul(commissionPay).div(100 ether);
        _transferToken(msg.sender, tokenAddress, to, value, _commission);
        cummulatedCommissions[tokenAddress] = cummulatedCommissions[tokenAddress].add(_commission);
        
        emit Pay(tokenAddress, msg.sender, nonceByUser[msg.sender], to, value, _commission, now);
    }
    
    function withdrawlCommission(address[] memory tokens) public onlyOwner {
        IRC223 token;
        
        for (uint i = 0; i < tokens.length; i++) {
            token = IRC223(tokens[i]);
            token.transfer(msg.sender, cummulatedCommissions[tokens[i]]);
            cummulatedCommissions[tokens[i]] = 0;
        }
        
        msg.sender.transfer(cummulatedCommissions[address(0)]);
        cummulatedCommissions[address(0)] = 0;
    }
    
    function _transfer(address _from, address payable to, uint value, uint _commission) internal {
        require(msg.value == value.add(_commission));
        to.transfer(value);
        nonceByUser[_from] = nonceByUser[_from].add(1);
    }
    
    function _transferToken(address _from, address tokenAddress, address to, uint value, uint _commission) internal {
        require(balances[_from][tokenAddress] == value.add(_commission));

        balances[_from][tokenAddress] = balances[_from][tokenAddress].sub(value.add(_commission));
        IRC223 token = IRC223(tokenAddress);
        token.transfer(to, value);
        nonceByUser[_from] = nonceByUser[_from].add(1);
    }
    
    function kill(address[] memory tokens) public onlyOwner {
        IRC223 token;
        
        for (uint i = 0; i < tokens.length; i++) {
            token = IRC223(tokens[i]);
            token.transfer(msg.sender, token.balanceOf(address(this)));
        }
        
        selfdestruct(msg.sender);
    }
}
sendGas = 76276
transferTokenGas = 116749
sendTokenGas = 93523

/**********************************************************************/
SEND PI
/**********************************************************************/
pimoney.methods.send("wallet_destino", "cantidad_sin_comision").send({from: user, value: cantidad_mas_comision, gasLimit: 76276});

/**********************************************************************/
PAY PI
/**********************************************************************/
pimoney.methods.send("wallet_destino", "cantidad_sin_comision").send({from: user, value: cantidad_mas_comision, gasLimit: 76276});

/**********************************************************************/
SEND TOKEN
/**********************************************************************/
token.transfer("address_contrato", "cantidad_mas_comision").send({from: user, gasLimit: 116749});
pimoney.methods.sendToken("token_address", "wallet_destino", "cantidad_sin_comision").send({from: user, gasLimit: 93523});

/**********************************************************************/
PAY TOKEN
/**********************************************************************/
token.transfer("address_contrato", "cantidad_mas_comision").send({from: user, gasLimit: 116749});
pimoney.methods.payToken("token_address", "wallet_destino", "cantidad_sin_comision").send({from: user, gasLimit: 93523});

/**********************************************************************/
GENERAL
/**********************************************************************/
//Comisión de envío...[0 - 100] %
pimoney.methods.commissionSend().call()

//Comisión de pago...[0 - 100] %
pimoney.methods.commissionPay().call()

/**********************************************************************/
EVENTOS
/**********************************************************************/

Send
0- tokenAddress (0x00..0 para Pi) INDEXABLE
1- sender INDEXABLE
2- nonce
3- receiver INDEXABLE
4- toSend
5- commission
6- time

Pay
0- tokenAddress (0x00..0 para Pi) INDEXABLE
1- sender INDEXABLE
2- nonce
3- receiver INDEXABLE
4- toSend
5- commission
6- time

INDEXABLE significa que cuando haces el getPastEvents puedes usar el filter: {} para filtrar por un valor de ese tipo
Ejemplo: 

contract.getPastEvents('Send', {
	filter: {tokenAddress: "0x...", sender: "0x..."},
	fromBlock: 0,
	toBlock: 'latest'
}, function(error, events) {
	if(error) {
		console.log(error);
	} else {
		console.log(events);
	}
});

Esto te devolverá un array de objetos de eventos de ese tokenAddress con ese sender. Ya una vez tienes eso puedes recorrer y filtrar tú por time por ejemplo.